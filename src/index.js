import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

//Création de 'app' en tant que composant qui renvoi un retour --> élément HTML
class App extends React.Component{

  //OBJET D'ETAT
  state = {
    listeDeSommes: [
    ],
    valeur1 : 0,
    valeur2 : 0,
    resultat : 0,
    compteur : 0
  };


handleSubmit = (evenement) => {
  evenement.preventDefault(); 
  const resultatSomme = this.state.valeur1 + this.state.valeur2;
  const listeDeSommes = this.state.listeDeSommes.slice();
  listeDeSommes.push({resultatSomme});

  if (listeDeSommes.length > 10) {
    listeDeSommes.shift();
  }

  this.setState({listeDeSommes: listeDeSommes, resultatSomme: resultatSomme});
  
}

//Récupération de la valeur du champ1
valeurChamp1 = (evenement) => {
  console.log("Valeur1 saisie : " + evenement.currentTarget.value)
  const valeur1 = Number.parseInt(evenement.currentTarget.value)
  this.setState({valeur1: valeur1})
}

//Récupération de la valeur du champ2
valeurChamp2 = (evenement) => {
  console.log("Valeur2 saisie : " + evenement.currentTarget.value)
  const valeur2 = Number.parseInt(evenement.currentTarget.value)
  this.setState({valeur2: valeur2})
}



  render() {

    const titre = "Exercice 1 : "
    const detailExercice = "Calculer la somme de 2 nombres saisis et l'afficher, puis sauvegarder les 10 derniers calculs"
    const texte = "Résultat : "
    const historique = "Historique"

    return  <div>
                <h1>{titre}</h1>

                <p>{detailExercice}</p>
                
                <form onSubmit={this.handleSubmit}> 

                    <input  type="number"  
                            onChange={this.valeurChamp1}  
                            placeholder="Nombre 1"/>

                    <input  type="number"  
                            onChange={this.valeurChamp2}  
                            placeholder="Nombre 2"/>

                    <button  > Valider </button>

                </form>

                <p>{texte}{this.state.resultat}</p>

                <ul>
                    <p>{historique}</p>

                    {this.state.listeDeSommes.map(somme => (
                      <li>{somme.resultatSomme}</li>
                    ))}

                </ul>

            </div>
  };
}


const rootElement = document.getElementById('root'); //creation var de type 'rootElement' qui est égale à id root

//demande de renvoi vers le DOM de React
ReactDOM.render(<App />, rootElement);
